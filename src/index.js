const { response, request } = require('express');
const express = require('express');
const emailController = require('./controllers/emailController');

const app = express();
const port = 3333;

app.use(express.json());

// Routes
app.get('/', (request, response) => {
    response.send('Working');
})

app.get('/email', async (request, response) => {
    response.send(await emailController.sendEmail());
});

app.listen(port, () => {
    console.log('Server is up and running on', port);
})