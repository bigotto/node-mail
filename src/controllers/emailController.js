const nodemailer = require('nodemailer');
const key = require('./key.json');
const serviceKey = require('./key.json');
const { google } = require("googleapis");
const OAuth2 = google.auth.OAuth2;
const jwtClient = new google.auth.JWT(
    serviceKey.client_email,
    null,
    serviceKey.private_key,
    ['https://mail.google.com'],
    'suporte@tarvos.ag'
)
// const oauth2Client = new OAuth2(
//     key.client_id,
//     key.client_secret,
//     "https://developers.google.com/oauthplayground"
// );
// oauth2Client.setCredentials({
//     refresh_token: key.refresh_token,
// });
// const { token } = await oauth2Client.getAccessToken();
async function sendEmail() {
    jwtClient.authorize(function (err, tokens) {
        let authService = {
            type: 'OAuth2',
            user: 'suporte@tarvos.ag',
            serviceClient: serviceKey.client_id,
            privateKey: serviceKey.private_key,
            accessToken: tokens.access_token,
            expires: tokens.expiry_date
        }
        let transporter;
        try {
            transporter = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 465,
                secure: true,
                auth: authService,
            });
        } catch (error) {
            console.log(error)
        }
        try {
            transporter.verify()
            transporter.sendMail({
                from: "any",
                to: 'negocios.matos@gmail.com',
                subject: 'Nodemailer test',
                text: 'I hope this message gets through!',
            });
        } catch (error) {
            console.log(error);
        }
        return 'email';
    })

}



module.exports = {
    sendEmail,
};
